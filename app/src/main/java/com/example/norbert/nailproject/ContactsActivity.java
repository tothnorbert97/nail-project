package com.example.norbert.nailproject;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class ContactsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private AdView mAdView;

    private Address addressCoordinate;

    private String address = "Dorog, István király u. 8, 2510";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        this.initVariables();
        this.loadAd();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        List<Address> address_list = null;
        Geocoder geocoder = new Geocoder(this);
        try {
            address_list = geocoder.getFromLocationName(address, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        addressCoordinate = address_list.get(0);
        LatLng latLng = new LatLng(addressCoordinate.getLatitude(), addressCoordinate.getLongitude());
        mMap.addMarker(new MarkerOptions().position(latLng).title(address));
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
        mMap.moveCamera(cameraUpdate);
    }

    public void btnBackOnClick(View view) {
        finish();
        startActivity(new Intent(ContactsActivity.this, MenuActivity.class));
    }

    private void initVariables() {
        this.mAdView = findViewById(R.id.adView);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mMap);
        mapFragment.getMapAsync(this);
    }

    private void loadAd() {
        this.mAdView.loadAd(new AdRequest.Builder().build());
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
