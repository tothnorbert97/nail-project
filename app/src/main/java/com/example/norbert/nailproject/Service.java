package com.example.norbert.nailproject;

/**
 * Created by Norbert on 2018.04.10..
 */

public class Service {
    private String name;
    private int price;
    private double length;

    public Service(){

    }

    public Service(String name, double length, int price) {
        this.name = name;
        this.length = length;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getLength() {
        return length;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return this.getName() + " : " + this.getLength() + " óra : " + this.getPrice() + " Ft";
    }
}
