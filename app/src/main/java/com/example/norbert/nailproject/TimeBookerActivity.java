package com.example.norbert.nailproject;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TimeBookerActivity extends AppCompatActivity {

    private ProgressBar pbBooker;

    private CalendarView cvCalendar;

    private Spinner spServices;
    private Spinner spTimes;

    private TextView tvInvalidTime;

    private Button btnSave;
    private Button btnBack;

    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd");
    private Date currentDate;

    public static Date selectedDate;
    public static int selectedServiceIdx;
    public static int selectedTimeIdx;

    public static List<StartEndTimePair> startEndTimePairs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_booker);

        this.initVariables();
        this.currentDate = new Date(cvCalendar.getDate());
        this.selectedDate = new Date(cvCalendar.getDate());
        this.uploadSpServices();

        this.cvCalendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                selectedDate = new Date(year - 1900, month, dayOfMonth);
                if (selectedDate.after(currentDate)) {
                    selectedDateIsValid();
                } else {
                    selectedDateIsInvalid();
                }
            }
        });

        this.spServices.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    spTimes.setEnabled(false);
                    spTimes.setAdapter(null);
                } else {
                    selectedServiceIdx = position - 1;
                    getBookedTimesOnCurrentDate(generateFreeTimesList(MenuActivity.servicesList.get(selectedServiceIdx).getLength()));
                    spTimes.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        this.spTimes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedTimeIdx = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void initVariables() {
        this.pbBooker = findViewById(R.id.pbBooker);

        this.cvCalendar = findViewById(R.id.cvCalendar);

        this.spServices = findViewById(R.id.spServices);
        this.spTimes = findViewById(R.id.spTimes);

        this.tvInvalidTime = findViewById(R.id.tvInvalidTime);

        this.btnSave = findViewById(R.id.btn_save);
        this.btnBack = findViewById(R.id.btnBack);
    }

    private void selectedDateIsInvalid() {
        this.tvInvalidTime.setVisibility(View.VISIBLE);
        this.spServices.setVisibility(View.INVISIBLE);
        this.spTimes.setVisibility(View.INVISIBLE);
        this.btnSave.setVisibility(View.INVISIBLE);
    }

    private void selectedDateIsValid() {
        this.tvInvalidTime.setVisibility(View.INVISIBLE);
        this.spServices.setVisibility(View.VISIBLE);
        this.spTimes.setVisibility(View.VISIBLE);
        this.btnSave.setVisibility(View.VISIBLE);
        this.spServices.setSelection(0);
        this.spTimes.setEnabled(false);
    }

    private void uploadSpServices() {
        List<String> services = new ArrayList<>();
        services.add("Szolgáltatások");
        for (Service service : MenuActivity.servicesList) {
            services.add(service.toString());
        }
        spServices.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, services));
    }

    private void uploadSpTimes(List<StartEndTimePair> freeTimes) {
        if (freeTimes.size() > 0) {
            List<String> freeTimesString = new ArrayList<>();
            for (StartEndTimePair freeTime : freeTimes) {
                freeTimesString.add(freeTime.toString());
            }
            spTimes.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, freeTimesString));
        } else {
            selectedDateIsInvalid();
        }
    }

    private List<StartEndTimePair> generateFreeTimesList(double selectedServiceLength) {
        List<StartEndTimePair> startEndTimePairList = new ArrayList<>();
        for (double i = 8; i + selectedServiceLength <= 20; i = i + 0.5) {
            startEndTimePairList.add(new StartEndTimePair(i, i + selectedServiceLength));
        }
        return startEndTimePairList;
    }

    private void getBookedTimesOnCurrentDate(final List<StartEndTimePair> freeTimes) {
        MainActivity.database.collection("booked").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                List<Time> bookedTimes = new ArrayList<>();
                List<DocumentSnapshot> documentSnapshotList = task.getResult().getDocuments();
                for (DocumentSnapshot documentSnapshot : documentSnapshotList) {
                    Time time = documentSnapshot.toObject(Time.class);
                    if (simpleDateFormat.format(currentDate).equals(time.getDate()) && time.getValidated()) {
                        bookedTimes.add(time);
                    }
                }
                uploadSpTimes(deleteBookedTimes(freeTimes, bookedTimes));
            }
        });
    }

    private List<StartEndTimePair> deleteBookedTimes(List<StartEndTimePair> freeTimes, List<Time> bookedTimes) {
        List<StartEndTimePair> definitiveFreeTimes = new ArrayList<>();
        for (StartEndTimePair freeTime : freeTimes) {
            definitiveFreeTimes.add(freeTime);
        }
        for (StartEndTimePair startEndTimePair : freeTimes) {
            for (Time booked : bookedTimes) {
                if ((startEndTimePair.startTime >= booked.getStart() && startEndTimePair.startTime < booked.getEnd()) || (startEndTimePair.endTime > booked.getStart() && startEndTimePair.endTime < booked.getEnd())) {
                    definitiveFreeTimes.remove(startEndTimePair);
                }
            }
        }
        startEndTimePairs = definitiveFreeTimes;
        return definitiveFreeTimes;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void btnSaveOnClick(View view) {
        if (selectedServiceIdx == 0) {
            Toast.makeText(TimeBookerActivity.this, "Nem választott ki szolgáltatást!", Toast.LENGTH_SHORT).show();
            return;
        } else {
            finish();
            startActivity(new Intent(TimeBookerActivity.this, TimeBookeValidatorActivity.class));
        }
    }

    public void btnBackOnClick(View view) {
        finish();
        startActivity(new Intent(TimeBookerActivity.this, MenuActivity.class));
    }
}
