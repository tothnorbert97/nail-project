package com.example.norbert.nailproject;

/**
 * Created by Norbert on 2018.04.05..
 */

public class User {
    private String phone_number;

    public User(){}

    public User(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
}
