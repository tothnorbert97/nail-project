package com.example.norbert.nailproject;

/**
 * Created by Norbert on 2018.04.05..
 */

public class Time {
    private String date, service;
    private Boolean validated;
    private double start;

    public double getEnd() {
        return end;
    }

    public void setEnd(double end) {
        this.end = end;
    }

    private double end;

    public Time(){

    }

    public Time(String date, String service, double start, double end ,Boolean validated) {
        this.date = date;
        this.service = service;
        this.validated = validated;
        this.start = start;
        this.end = end;
    }

    public String getDate() {
        return date;
    }

    public double getStart() {
        return start;
    }

    public void setStart(double start) {
        this.start = start;
    }

    public String getService() {
        return service;
    }

    public Boolean getValidated() {
        return validated;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public void setService(String service) {
        this.service = service;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }
}
