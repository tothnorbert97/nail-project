package com.example.norbert.nailproject;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class MenuActivity extends AppCompatActivity {

    private ProgressBar pbMenu;

    private AdView adView;

    public static User userData;
    public static Time userTime;
    public static List<Service> servicesList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        this.initVariables();
        this.loadAd();
    }

    public void btnMyDataOnClick(View view) {
        pbMenu.setVisibility(View.VISIBLE);
        MainActivity.database.collection("users").document(MainActivity.currentUser.getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                userData = documentSnapshot.toObject(User.class);
                pbMenu.setVisibility(View.INVISIBLE);
                finish();
                startActivity(new Intent(MenuActivity.this, MyDatasActivity.class));
            }
        });
    }

    public void btnMyBookedTimeOnClick(View view) {
        pbMenu.setVisibility(View.VISIBLE);
        MainActivity.database.collection("booked").document(MainActivity.currentUser.getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                userTime = documentSnapshot.toObject(Time.class);
                pbMenu.setVisibility(View.INVISIBLE);
                if (userTime != null) {
                    finish();
                    startActivity(new Intent(MenuActivity.this, MyBookedTimeActivity.class));
                } else {
                    Toast.makeText(MenuActivity.this,"Még nem foglalt időpontot!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void btnTimeBookerOnClick(View view) {
        pbMenu.setVisibility(View.INVISIBLE);
        MainActivity.database.collection("booked").document(MainActivity.currentUser.getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                userTime = documentSnapshot.toObject(Time.class);
                pbMenu.setVisibility(View.INVISIBLE);
                if (userTime == null) {
                    MainActivity.database.collection("services").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            List<DocumentSnapshot> documents = task.getResult().getDocuments();
                            for (DocumentSnapshot item : documents){
                                servicesList.add(item.toObject(Service.class));
                            }
                            finish();
                            startActivity(new Intent(MenuActivity.this, TimeBookerActivity.class));
                        }
                    });
                } else {
                    Toast.makeText(MenuActivity.this,"Már van foglalt időpontja!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void btnContactsOnClick(View view) {
        startActivity(new Intent(MenuActivity.this, ContactsActivity.class));
    }

    public void btnLogoutOnClick(View view) {
        MainActivity.mAuth.signOut();
        finish();
        startActivity(new Intent(MenuActivity.this, MainActivity.class));
    }

    private void loadAd() {
        MobileAds.initialize(this, "ca-app-pub-2486473224535115~1794279975");
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    private void initVariables() {
        this.pbMenu = findViewById(R.id.pbMenu);

        this.adView = findViewById(R.id.adView);

        servicesList = new ArrayList<Service>();
    }

    @Override
    public void onBackPressed() {
        btnLogoutOnClick(null);
    }
}
