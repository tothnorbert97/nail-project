package com.example.norbert.nailproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MyBookedTimeActivity extends AppCompatActivity {

    private TextView tvDate;
    private TextView tvTime;
    private TextView tvService;
    private TextView tvInvalidTime;

    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booked_time);

        this.initVariables();
        this.loadAd();
        this.uploadTextViewsWithData();
    }

    private void initVariables() {
        this.tvDate = findViewById(R.id.tvDate);
        this.tvTime = findViewById(R.id.tvTime);
        this.tvService = findViewById(R.id.tvService);
        this.tvInvalidTime = findViewById(R.id.tvInvalidTime);

        this.adView = findViewById(R.id.adView);
    }

    private void loadAd() {
        adView.loadAd(new AdRequest.Builder().build());
    }

    private void uploadTextViewsWithData() {
        this.tvDate.setText(MenuActivity.userTime.getDate());
        this.tvTime.setText(new StartEndTimePair(MenuActivity.userTime.getStart(),MenuActivity.userTime.getEnd()).toString());
        this.tvService.setText(MenuActivity.userTime.getService());
        if (MenuActivity.userTime.getValidated().equals(true)) {
            this.tvInvalidTime.setText("Az időpontod el lett fogadva!");
        } else {
            this.tvInvalidTime.setText("Az időpontod még nem lett elfogadva!");
        }
    }

    public void btnDeleteOnClick(View view) {
        finish();
        startActivity(new Intent(MyBookedTimeActivity.this, TimeDeleteValidatorActivity.class));
    }

    public void btnBackOnClick(View view) {
        finish();
        startActivity(new Intent(MyBookedTimeActivity.this, MenuActivity.class));
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
