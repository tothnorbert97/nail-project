package com.example.norbert.nailproject;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class NewPasswordActivity extends AppCompatActivity {

    private EditText etEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);

        this.initVariables();;
    }

    private void initVariables(){
        this.etEmail = findViewById(R.id.etEmail);
    }

    public void btnSendOnClick(View view) {
        MainActivity.mAuth.sendPasswordResetEmail(etEmail.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Toast.makeText(NewPasswordActivity.this,"Jelszó változtató e-mail elküldve!",Toast.LENGTH_SHORT).show();
                    finish();
                    startActivity(new Intent(NewPasswordActivity.this,LoginActivity.class));
                }
                else{
                    Toast.makeText(NewPasswordActivity.this,"Küldés sikertelen! Kérem próbálja később!",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void btnBackOnClick(View view) {
        finish();
        startActivity(new Intent(NewPasswordActivity.this,LoginActivity.class));
    }
}
