package com.example.norbert.nailproject;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.UserProfileChangeRequest;

public class MyDatasActivity extends AppCompatActivity {

    private ProgressBar pbMyData;

    private EditText etName;
    private EditText etPhone;

    private TextView tvEmail;

    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_datas);

        this.initVariables();
        this.loadAd();

        this.setEtNameDefaultText();
        this.setTvEmailDefaultText();
        this.setEtPhoneDefaultText();
    }

    private void initVariables() {
        this.pbMyData = findViewById(R.id.pbMyData);

        this.etName = findViewById(R.id.etName);
        this.etPhone = findViewById(R.id.etPhone);

        this.tvEmail = findViewById(R.id.tvEmail);

        this.adView = findViewById(R.id.adView);
    }

    private void loadAd() {
        this.adView.loadAd(new AdRequest.Builder().build());
    }

    private void setEtNameDefaultText() {
        this.etName.setText(MainActivity.currentUser.getDisplayName());
    }

    private void setTvEmailDefaultText() {
        this.tvEmail.setText(MainActivity.currentUser.getEmail());
    }

    private void setEtPhoneDefaultText() {
        this.etPhone.setText(MenuActivity.userData.getPhone_number());
    }

    @NonNull
    private Boolean saveChanges(String name, String phone_number) {

        if (!name.equals(MainActivity.currentUser.getDisplayName()) && !name.isEmpty()) {
            MainActivity.currentUser.updateProfile(new UserProfileChangeRequest.Builder().setDisplayName(name).build());
        } else if (name.isEmpty()) {
            Toast.makeText(MyDatasActivity.this, "Nem adott meg nevet!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!phone_number.equals(MenuActivity.userData.getPhone_number()) && !phone_number.isEmpty()) {
            if (phone_number.length() == 12) {
                MenuActivity.userData.setPhone_number(phone_number);
                MainActivity.database.collection("users").document(MainActivity.currentUser.getUid()).set(MenuActivity.userData);
            } else {
                Toast.makeText(MyDatasActivity.this, "Nem jól adta meg a telefonszámot!", Toast.LENGTH_SHORT).show();
                return false;
            }
        } else if (phone_number.isEmpty()) {
            Toast.makeText(MyDatasActivity.this, "Nem adott meg telefonszámot!", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void goBack() {
        finish();
        startActivity(new Intent(MyDatasActivity.this, MenuActivity.class));
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void btnSaveOnClick(View view) {
        pbMyData.setVisibility(View.VISIBLE);
        if (saveChanges(etName.getText().toString(), etPhone.getText().toString())) {
            Toast.makeText(MyDatasActivity.this, "Adatok megváltoztatása sikeres!", Toast.LENGTH_SHORT).show();
            pbMyData.setVisibility(View.INVISIBLE);
            goBack();
        }else{
            pbMyData.setVisibility(View.INVISIBLE);
        }
    }

    public void btnBackOnClick(View view) {
        goBack();
    }

    public void btnNewPasswordOnClick(View view) {
        MainActivity.mAuth.sendPasswordResetEmail(tvEmail.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(MyDatasActivity.this, "Jelszó változtató e-mail elküldve!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MyDatasActivity.this, "Küldés sikertelen! Kérem próbálja később!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
