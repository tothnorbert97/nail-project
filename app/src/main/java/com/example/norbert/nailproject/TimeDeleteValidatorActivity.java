package com.example.norbert.nailproject;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

public class TimeDeleteValidatorActivity extends AppCompatActivity {

    private ProgressBar pbDelete;

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_delete_validator);

        this.initVariables();
        this.loadAd();
    }

    private void initVariables() {
        this.pbDelete = findViewById(R.id.pbDelete);

        this.mAdView = findViewById(R.id.adView);
    }

    private void loadAd() {
        this.mAdView.loadAd(new AdRequest.Builder().build());
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void btnYesOnClick(View view) {
        pbDelete.setProgress(View.VISIBLE);
        MainActivity.database.collection("booked").document(MainActivity.currentUser.getUid()).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                pbDelete.setProgress(View.INVISIBLE);
                finish();
                startActivity(new Intent(TimeDeleteValidatorActivity.this, MenuActivity.class));
            }
        });
    }

    public void btnNoOnClick(View view) {
        finish();
        startActivity(new Intent(TimeDeleteValidatorActivity.this, MyBookedTimeActivity.class));
    }
}
