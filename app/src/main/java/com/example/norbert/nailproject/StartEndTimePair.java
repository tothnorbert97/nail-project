package com.example.norbert.nailproject;

/**
 * Created by Norbert on 2018.05.01..
 */

public class StartEndTimePair {
    double startTime, endTime;

    public StartEndTimePair(double startTime, double endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public double getStartTime() {
        return startTime;
    }

    public double getEndTime() {
        return endTime;
    }

    @Override
    public String toString() {
        return String.valueOf((int) startTime) + (startTime % 1 == 0 ? ":00" : ":30") + " - " + String.valueOf((int) endTime) + (endTime % 1 == 0 ? ":00" : ":30");
    }
}
