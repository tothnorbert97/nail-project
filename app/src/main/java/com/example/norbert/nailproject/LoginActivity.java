package com.example.norbert.nailproject;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

public class LoginActivity extends AppCompatActivity {

    private ProgressBar pbLogin;

    private EditText etEmail;
    private EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.initVariables();
    }

    private void initVariables() {
        this.pbLogin = findViewById(R.id.pbLogin);

        this.etEmail = findViewById(R.id.etEmail);
        this.etPassword = findViewById(R.id.etPassword);
    }

    public void btnLoginOnClick(View view) {
        pbLogin.setVisibility(View.VISIBLE);
        if (canLogin(etEmail.getText().toString(), etPassword.getText().toString())) {
            MainActivity.mAuth.signInWithEmailAndPassword(etEmail.getText().toString(), etPassword.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    pbLogin.setVisibility(View.INVISIBLE);
                    if (task.isSuccessful()) {
                        MainActivity.currentUser = MainActivity.mAuth.getCurrentUser();
                        if (MainActivity.currentUser.isEmailVerified()) {
                            startActivity(new Intent(LoginActivity.this, MenuActivity.class));
                        } else {
                            Toast.makeText(LoginActivity.this, "Az e-mail cím nincs megerősítve!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, "Rossz e-mail cím/jelszó!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    public void btnRegistrationOnClick(View view) {
        finish();
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
    }

    public void btnChangePasswordOnClick(View view) {
        finish();
        startActivity(new Intent(LoginActivity.this, NewPasswordActivity.class));
    }

    @NonNull
    private Boolean canLogin(String email, String password) {
        if (email.isEmpty()) {
            Toast.makeText(LoginActivity.this, "Kérem adja meg az e-mail címét!", Toast.LENGTH_SHORT).show();
            pbLogin.setVisibility(View.INVISIBLE);
            return false;
        } else if (password.isEmpty()) {
            Toast.makeText(LoginActivity.this, "Kérem adja meg a jelszavát!", Toast.LENGTH_SHORT).show();
            pbLogin.setVisibility(View.INVISIBLE);
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
