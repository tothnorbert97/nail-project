package com.example.norbert.nailproject;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;

public class TimeBookeValidatorActivity extends AppCompatActivity {

    private ProgressBar pbBookerValidate;

    private TextView tvDate;
    private TextView tvService;
    private TextView tvTime;

    private AdView mAdView;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_booker_validator);

        this.initVariables();
        this.setTextViewsData();
        this.loadAd();

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                Toast.makeText(TimeBookeValidatorActivity.this, "Időpont foglalási kérelem elküldve!", Toast.LENGTH_SHORT);
                finish();
                startActivity(new Intent(TimeBookeValidatorActivity.this, MenuActivity.class));
            }
        });
    }

    private void initVariables() {
        this.pbBookerValidate = findViewById(R.id.pbBookerValidate);

        this.tvDate = findViewById(R.id.tvDate);
        this.tvService = findViewById(R.id.tvService);
        this.tvTime = findViewById(R.id.tvTime);

        this.mAdView = findViewById(R.id.adView);
        this.mInterstitialAd = new InterstitialAd(this);
    }

    private void setTextViewsData() {
        this.tvDate.setText(TimeBookerActivity.simpleDateFormat.format(TimeBookerActivity.selectedDate));
        this.tvService.setText(MenuActivity.servicesList.get(TimeBookerActivity.selectedServiceIdx).getName());
        this.tvTime.setText(TimeBookerActivity.startEndTimePairs.get(TimeBookerActivity.selectedTimeIdx).toString());
    }

    private void loadAd() {
        mAdView.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void btnYesOnClick(View view) {
        pbBookerValidate.setVisibility(View.VISIBLE);
        Time selectedTime = new Time(TimeBookerActivity.simpleDateFormat.format(TimeBookerActivity.selectedDate), MenuActivity.servicesList.get(TimeBookerActivity.selectedServiceIdx).getName(), TimeBookerActivity.startEndTimePairs.get(TimeBookerActivity.selectedTimeIdx).getStartTime(), TimeBookerActivity.startEndTimePairs.get(TimeBookerActivity.selectedTimeIdx).getEndTime(), false);
        MainActivity.database.collection("booked").document(MainActivity.currentUser.getUid()).set(selectedTime).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    pbBookerValidate.setVisibility(View.INVISIBLE);
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                } else {
                    Toast.makeText(TimeBookeValidatorActivity.this, "Időpont foglalás sikertelen! Próbálja újra!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void btnNoOnClick(View view) {
        finish();
        startActivity(new Intent(TimeBookeValidatorActivity.this, TimeBookerActivity.class));
    }
}
