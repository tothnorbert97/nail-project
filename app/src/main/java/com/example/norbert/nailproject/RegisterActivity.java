package com.example.norbert.nailproject;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.UserProfileChangeRequest;

public class RegisterActivity extends AppCompatActivity {

    private ProgressBar pb_registration;

    private EditText etName;
    private EditText etEmail;
    private EditText etPhone;
    private EditText etPassword;
    private EditText etPasswordAgain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        this.initVariables();
    }

    private void initVariables() {
        this.pb_registration = findViewById(R.id.pbRegistration);

        this.etName = findViewById(R.id.etName);
        this.etEmail = findViewById(R.id.etEmail);
        this.etPhone = findViewById(R.id.etPhone);
        this.etPassword = findViewById(R.id.etPassword);
        this.etPasswordAgain = findViewById(R.id.etPasswordAgain);
    }

    public void btnRegistrationOnClick(View view) {
        pb_registration.setVisibility(View.VISIBLE);

        final String name = etName.getText().toString();
        final String email = etEmail.getText().toString();
        final String phone = etPhone.getText().toString();
        final String password = etPassword.getText().toString();
        final String password_again = etPasswordAgain.getText().toString();


        if (canRegistration(name, email, phone, password, password_again)) {
            MainActivity.mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        MainActivity.mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    MainActivity.mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            MainActivity.currentUser = MainActivity.mAuth.getCurrentUser();
                                            MainActivity.currentUser.updateProfile(new UserProfileChangeRequest.Builder().setDisplayName(name).build());
                                            MainActivity.database.collection("users").document(MainActivity.currentUser.getUid()).set(new User(phone)).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()){
                                                        MainActivity.currentUser.sendEmailVerification();
                                                        Toast.makeText(RegisterActivity.this, "Sikeres regisztráció!", Toast.LENGTH_SHORT).show();
                                                        goBack();
                                                    }else{
                                                        Toast.makeText(RegisterActivity.this,"Regisztráció sikertelen!",Toast.LENGTH_SHORT).show();
                                                        MainActivity.currentUser.delete();
                                                    }
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        });
                    } else {
                        pb_registration.setVisibility(View.INVISIBLE);
                        Toast.makeText(RegisterActivity.this, "Ezt az e-mail címet már regisztrálták!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }else{
            pb_registration.setVisibility(View.INVISIBLE);
        }
    }

    public void btnRegisteredOnClick(View view) {
        goBack();
    }

    @NonNull
    private Boolean canRegistration(String name, String email, String phone, String password, String password_again) {
        if (name.isEmpty()) {
            Toast.makeText(RegisterActivity.this, "Nem adott meg nevet!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (email.isEmpty()) {
            Toast.makeText(RegisterActivity.this, "Nem adott meg e-mail címet!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (phone.isEmpty()) {
            Toast.makeText(RegisterActivity.this, "Nem adott meg telefonszámot!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (password.isEmpty()) {
            Toast.makeText(RegisterActivity.this, "Nem adott meg jelszót!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (password.length() < 6) {
            Toast.makeText(RegisterActivity.this, "A megadott jelszó túl rövid!", Toast.LENGTH_SHORT).show();
            etPassword.setText("");
            etPasswordAgain.setText("");
            return false;
        }

        if (!password.equals(password_again)) {
            Toast.makeText(RegisterActivity.this, "A jelszavak nem egyeznek meg!", Toast.LENGTH_SHORT).show();
            etPassword.setText("");
            etPasswordAgain.setText("");
            return false;
        }

        return true;
    }

    private void goBack(){
        finish();
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
