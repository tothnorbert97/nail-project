package com.example.norbert.nailproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;

public class MainActivity extends AppCompatActivity {

    public static FirebaseAuth mAuth;
    public static FirebaseUser currentUser;
    public static FirebaseFirestore database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        currentUser = mAuth.getCurrentUser();

        database = FirebaseFirestore.getInstance();

        LoggedIn();
    }

    private void LoggedIn(){
        finish();
        if (currentUser != null && currentUser.isEmailVerified()){
            startActivity(new Intent(MainActivity.this, MenuActivity.class));
        }
        else{
            startActivity(new Intent(MainActivity.this,LoginActivity.class));
        }
    }
}
